from airflow.operators.dummy_operator import DummyOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from airflow.providers.apache.hive.sensors.hive_partition import HivePartitionSensor
from airflow.utils.task_group import TaskGroup

def knowledge_gaps_sensor(mediawiki_snapshot, wikidata_snapshot):

    with TaskGroup("knowledge_gaps_sensor") as sensor_group:

        # patience is a virtue
        timeout = 60 * 60 * 24 * 30
        poke_interval = 60 * 60

        # ~11 days delay
        wait_for_wikidata_item_page_link_snapshot = NamedHivePartitionSensor(
            task_id='wait_for_wikidata_item_page_link_snapshot',
            partition_names=[f'wmf.wikidata_item_page_link/snapshot={wikidata_snapshot}'],
            poke_interval=poke_interval,
            timeout=timeout
        )

        # ~11 days delay
        wait_for_wikidata_entity_snapshot = NamedHivePartitionSensor(
            task_id='wait_for_wikidata_entity_snapshot',
            partition_names=[f'wmf.wikidata_entity/snapshot={wikidata_snapshot}'],
            poke_interval=poke_interval,
            timeout=timeout
        )

        # ~2 days delay
        wait_for_mediawiki_page_history_snapshot = NamedHivePartitionSensor(
            task_id='wait_for_mediawiki_page_history_snapshot',
            partition_names=[f'wmf.mediawiki_page_history/snapshot={mediawiki_snapshot}'],
            poke_interval=poke_interval,
            timeout=timeout
        )

        # ~1 day (sometimes longer)
        wait_for_mediawiki_revision_snapshot = HivePartitionSensor(
            task_id='wait_for_mediawiki_revision_snapshot',
            table='wmf_raw.mediawiki_revision',
            partition=f"snapshot='{mediawiki_snapshot}'",
            poke_interval=poke_interval,
            timeout=timeout
        )

        # ~1 day (sometimes longer)
        wait_for_mediawiki_page_snapshot = HivePartitionSensor(
            task_id='wait_for_mediawiki_page_snapshot',
            table='wmf_raw.mediawiki_page',
            partition=f"snapshot='{mediawiki_snapshot}'",
            poke_interval=poke_interval,
            timeout=timeout
        )

        task_start = DummyOperator(
            task_id='await_sensors',
        )
        task_end = DummyOperator(
            task_id='knowledge_gaps_triggered'
        )

        task_start >> [
            wait_for_wikidata_item_page_link_snapshot,
            wait_for_wikidata_entity_snapshot,
            wait_for_mediawiki_page_history_snapshot,
            wait_for_mediawiki_revision_snapshot,
            wait_for_mediawiki_page_snapshot
        ] >> task_end

    return sensor_group