"""A DAG to execute the full knowledge gaps pipeline.
"""

from datetime import datetime, timedelta
import os

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator

from research.config import dag_config
from research.dags.snapshot_sensor import knowledge_gaps_sensor
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "knowledge_gaps_pipeline"
var_props = VariableProperties(f"{dag_id}_config")

# snapshots that the sensors will wait for
mediawiki_snapshot_template = '{{data_interval_start | start_of_previous_month | to_ds_month}}'
wikidata_snapshot_template = '{{data_interval_start | end_of_previous_month | start_of_current_week | to_ds}}'
mediawiki_snapshot = var_props.get('mediawiki_snapshot', mediawiki_snapshot_template)
wikidata_snapshot = var_props.get('wikidata_snapshot', wikidata_snapshot_template)

# optionally use a conda environment built from a branch
if var_props.get('conda_version', None):
    knowledge_gaps_version = var_props.get('conda_version', None)
    knowledge_gaps_conda_env_name = f"knowledge-gaps-{knowledge_gaps_version}.conda"
    conda_environment = f"https://gitlab.wikimedia.org/api/v4/projects/212/packages/generic/knowledge-gaps/{knowledge_gaps_version}/{knowledge_gaps_conda_env_name}.tgz#venv"
else:
    conda_environment = dag_config.artifact('knowledge_gaps.tgz')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 1, 1)),
    schedule='@monthly',
    dagrun_timeout=timedelta(days=30),
    catchup=False,
    user_defined_filters=filters,
    tags=['spark', 'hive', 'research', 'knowledge gaps'],
    default_args={
        **var_props.get_merged("default_args", dag_config.default_args),
    }
) as dag:
    # for production, the airflow variable has to be set to "production"
    mode = var_props.get('mode', 'development')

    hdfs_temp_directory = var_props.get('hdfs_temp_directory', dag_config.hdfs_temp_directory)
    run_dir = "{{data_interval_start | to_ds_nodash}}"
    hdfs_dir = os.path.join(hdfs_temp_directory, "knowledge_gaps", run_dir)
    hdfs_dir = var_props.get('hdfs_dir', hdfs_dir)

    output_database = var_props.get('output_database', 'knowledge_gaps_dev')
    table_prefix = var_props.get('table_prefix', 'output_')

    start_time_bucket = var_props.get('start_time_bucket', '20050101')
    end_time_bucket = var_props.get('end_time_bucket', '{{data_interval_start | start_of_next_month | to_ds_nodash}}')

    time_bucket_freq = var_props.get('time_bucket_freq', 'monthly')

    # development mode configuration
    dev_input_config = []
    content_gaps_to_generate = []
    if mode == "development":
        dev_input_config.extend([
            "--dev_database_name", var_props.get('dev_database_name', 'knowledge_gaps_dev'),
            "--dev_table_prefix", var_props.get('dev_table_prefix', 'dev_'),
        ])

        if var_props.get('content_gaps', None):
            content_gaps_to_generate = [ "--content_gaps", var_props.get('content_gaps', None) ]

    common_params = {
        'launcher': 'skein',
        'driver_memory': '4G',
        # As of 07/2022 skein memory is not configurable. Turn
        # logging off in order to avoid errors like
        # "skein.exceptions.DriverError: Received message larger than
        # max (6810095 vs. 4194304)".
        'skein_app_log_collection_enabled': False if mode=="production" else True,
        # to keep the skein containers running for development
        # 'command_postamble': '; sleep 600',
    }

    spark_conf = {
        'spark.shuffle.service.enabled': 'true',
        'spark.executor.memoryOverhead': '2048',
        # 'spark.sql.sources.partitionOverwriteMode': 'dynamic',
        'spark.dynamicAllocation.enabled': 'true',
        # 'spark.dynamicAllocation.initialExecutors': 5,
    }

    def compute_feature_metrics():
        if var_props.get('skip_metric_features', False):
            return DummyOperator(task_id='metric_features_skipped')

        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 100 if mode=="production" else 10,
            'spark.sql.shuffle.partitions': 4000 if mode=="production" else 20,
        }
        return  SparkSubmitOperator.for_virtualenv(
            task_id='metric_features',
            **common_params,
            executor_memory='14G' if mode=="production" else '4G',
            executor_cores=4,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive = conda_environment,
            entry_point = f"bin/knowledge_gaps_feature_metrics.py",
            application_args=[

                start_time_bucket,
                end_time_bucket,
                "--time_bucket_freq", time_bucket_freq,
                "--mediawiki_snapshot", mediawiki_snapshot,
                "--mode", mode,
                "--hdfs_dir", hdfs_dir
            ] + dev_input_config,
        )

    def compute_content_features():
        if var_props.get('skip_content_gap_features', False):
            return DummyOperator(task_id='content_gap_features_skipped')

        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 120 if mode=="production" else 10,
            'spark.sql.shuffle.partitions': 2000 if mode=="production" else 20,
        }
        return SparkSubmitOperator.for_virtualenv(
            task_id='content_gap_features',
            **common_params,
            executor_memory='14G' if mode=="production" else '4G',
            executor_cores=2,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive = conda_environment,
            entry_point = f"bin/knowledge_gaps_content_features.py",
            application_args=[

                "--mediawiki_snapshot", mediawiki_snapshot,
                "--wikidata_snapshot", wikidata_snapshot,
                "--mode", mode,
                "--hdfs_dir", hdfs_dir
            ] + dev_input_config + content_gaps_to_generate,
        )

    def compute_content_gap_metrics():
        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 100 if mode=="production" else 10,
            'spark.sql.shuffle.partitions': 400 if mode=="production" else 20,
        }
        return SparkSubmitOperator.for_virtualenv(
            task_id='content_gap_metrics',
            **common_params,
            executor_memory='14G' if mode=="production" else '4G',
            executor_cores=4,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive = conda_environment,
            entry_point = f"bin/knowledge_gaps_aggregation.py",
            application_args=[

                "--hdfs_dir", hdfs_dir,
            ] + content_gaps_to_generate,
        )

    def generate_output_datasets():
        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 40 if mode=="production" else 4,
            'spark.sql.shuffle.partitions': 20 if mode=="production" else 2
        }
        prefix = [] if table_prefix == "" else ["--table_prefix", table_prefix,]
        return SparkSubmitOperator.for_virtualenv(
            task_id='output_datasets',
            **common_params,
            executor_memory='8G',
            executor_cores=4,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive = conda_environment,
            entry_point = f"bin/knowledge_gaps_datasets.py",
            application_args=[
                "--hdfs_dir", hdfs_dir,
                "--database_name", output_database,
                "--store_metric_features",
                "--store_content_gap_features",
                "--store_normalized_metrics",
                "--store_denormalized_metrics",
                "--store_knowledge_gap_index_metrics",
            ] + prefix,
        )

    knowledge_gaps_sensor(mediawiki_snapshot, wikidata_snapshot) >> \
    [compute_feature_metrics(), compute_content_features() ] >> \
    compute_content_gap_metrics() >> \
    generate_output_datasets()
