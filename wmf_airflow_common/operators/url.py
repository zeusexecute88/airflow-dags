import fsspec
from airflow.models import BaseOperator
from typing import Any
from workflow_utils.util import fsspec_use_new_pyarrow_api
from urllib.parse import urlparse

fsspec_use_new_pyarrow_api(should_set_hadoop_env_vars=True)


def fsspec_touch(url: str) -> bool:
    """
    Touches the file at URL.
    This only works with URLs that are supported by fsspec on writeable file systems.

    :param url:
        A URL to touch.
    """
    # touch is a method on fsspec FileSystem, so
    # we 'open' the file (with fsspec, not python file IO),
    # get the fsspec FileSystem, and then
    # make FileSystem touch the url path on that FileSystem.
    url_parsed = urlparse(url)
    return bool(fsspec.open(url).fs.touch(url_parsed.path))

def fsspec_delete(url: str, recursive: bool =False) -> bool:
    """
    Deletes the file at URL

    :param url:
        A path to delete
    :param recursive:
        If path is directory, recursively delete contents and then
        also remove the directory.
        Default value is False.
    """
    url_parsed = urlparse(url)
    return bool(fsspec.open(url).fs.delete(url_parsed.path, recursive))

def directory_has_size(url: str) -> bool:
    """
    Gets the size of a directory

    :param url:
        A URL to check size.
    """
    url_parsed = urlparse(url)
    directory_size=int(fsspec.open(url).fs.du(url_parsed.path))
    if (directory_size > 0):
        return True
    else:
        return False


class URLTouchOperator(BaseOperator):
    """
    Uses fsspec client to touch output files.
    This is useful for writing _SUCCESS flags or any other file.
    It is used to mediate between jobs that are already on Oozie and those we are still migrating to airflow.
    """

    template_fields = ('_url',)


    def __init__(self,*, url: str, **kwargs: Any):
        """
        :param_url:
          url to write to
        """
        self._url = url

        super().__init__(**kwargs)

    def execute(self, context: Any) -> None:
        # Creates an empty file
        fsspec_touch(self._url)

class URLDeleteOperator(BaseOperator):
    """
    Uses fsspec client to delete files.
    """

    template_fields = (
        '_url',
    )
    def __init__(self,*, url: str, recursive: bool=False, **kwargs: Any):
        """
        :param_url:
          url to delete

        :param recursive:
            whether to recursively delete contents and then
            also remove the directory if url is a directory.
        """
        self._url = url
        self._recursive = recursive

        super().__init__(**kwargs)

    def execute(self, context: Any) -> None:
        # Deletes a file or directory
        fsspec_delete(self._url, self._recursive)
