from typing import Any, Optional
import os
import logging
from mergedeep import merge
import shutil
from wmf_airflow_common.config import dag_default_args


def get_base_spark2_defaults_args() -> dict:
    """
    Gets values for some Spark operator defaults we might always want to set.
    """
    spark_default_args: dict[str, Any] = {}

    # Set spark_home in default_args, defaulting to /usr/lib/spark2 if it exists.
    spark_home_default = '/usr/lib/spark2'
    spark_home = os.environ.get('SPARK_HOME')
    if spark_home is None and os.path.exists(spark_home_default):
        spark_home = spark_home_default
    if spark_home is not None:
        spark_default_args['spark_home'] = spark_home

    # Default to running spark in yarn client mode if hadoop is available.
    # (Assuming this means hadoop is configured for use with Spark by default.)
    if shutil.which('hadoop') is not None:
        spark_default_args['master'] = 'yarn'
        spark_default_args['deploy_mode'] = 'client'

    # Notice: These vars are set in all spark processes, including executors.
    spark_default_args['env_vars'] = {'SPARK_CONF_DIR': '/etc/spark2/conf'}

    # This archive has been manually uploaded to HDFS.
    spark_default_args['conf'] = {
        'spark.yarn.archive': 'hdfs:///user/spark/share/lib/spark-2.4.4-assembly.zip',
    }

    return spark_default_args


def get(extra_default_args: Optional[dict] = None) -> dict:
    """
    Gets the deprecated Spark2 flavour of the operator default_args for airflow instance_name.

    Overloads the wmf_airflow_common/config/dag_default_args with Spark2 specific configuration.

    :param extra_default_args:
        These will be merged over any of the variant's default_args.
        Dicts are deep merged, any other type is replaced on conflict.
    """
    logging.info('Overriding standard configuration with Spark2 deprecated ones.')

    default_args = dag_default_args.get(extra_default_args)
    # Delete incoming Spark3 related configs, if any
    default_args.pop('spark_home', None)
    default_args.pop('spark_binary', None)

    common_default_args: dict = merge(
        {},
        default_args,
        get_base_spark2_defaults_args()
    )

    return common_default_args
