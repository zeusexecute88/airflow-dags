from wmf_airflow_common.config.variable_properties import VariableProperties
import unittest.mock as mock
from pytest import raises


def test_get_list(monkeypatch):
    json_var = '{"a_list_var": ["a", "b"], "not_a_list_var": 1}'
    with mock.patch.dict('os.environ', AIRFLOW_VAR_MYDAG_CONFIG=json_var):
        varprop = VariableProperties('mydag_config')
        assert varprop.get_list('a_list_var', []) == ['a', 'b']
        assert varprop.get_list('missing_var', ['a_default']) == ['a_default']
        with raises(ValueError):
            varprop.get_list('not_a_list_var', [])
