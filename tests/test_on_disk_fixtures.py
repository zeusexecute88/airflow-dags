import pytest

from wmf_airflow_common.operators.skein import SkeinOperator, SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator


@pytest.mark.for_each_task(kind=(SkeinOperator, SimpleSkeinOperator), instance_name=None)
def test_skein_op_against_fixtures(
    request, task, render_task, compare_with_fixture
):
    rendered_task = render_task(task)
    skein_hook = rendered_task.make_hook()

    compare_with_fixture(
        group='skein_operator_spec',
        fixture_id=request.node.callspec.id,
        serde='str',
        content=skein_hook._application_spec.to_yaml())


@pytest.mark.for_each_task(kind=SparkSubmitOperator, instance_name=None)
def test_spark_cli_args_against_fixtures(
    request, task, render_task, compare_with_fixture
):
    rendered_task = render_task(task)
    kwargs = {}
    if task._launcher == 'skein':
        skein_hook = rendered_task._get_hook()._skein_hook
        kwargs = {
            'serde': 'str',
            'content': skein_hook._application_spec.to_yaml(),
        }
    else:
        spark_hook = rendered_task._get_hook()
        kwargs = {
            'serde': 'json',
            'content': spark_hook._build_spark_submit_command()
        }

    compare_with_fixture(
        group='spark_skein_specs',
        fixture_id=request.node.callspec.id,
        **kwargs)
