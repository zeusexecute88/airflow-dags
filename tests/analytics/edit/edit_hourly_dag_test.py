import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'edit', 'edit_hourly_dag.py']


def test_edit_hourly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="edit_hourly")
    assert dag is not None
    assert len(dag.tasks) == 2
