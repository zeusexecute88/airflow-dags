import pytest

# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'mediawiki', 'wikitext', 'mediawiki_wikitext_current_dag.py']


def test_mediawiki_wikitext_current_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id='mediawiki_wikitext_current')
    assert dag is not None
    assert len(dag.tasks) == 4