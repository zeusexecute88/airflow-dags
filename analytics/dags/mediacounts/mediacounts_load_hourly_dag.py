'''
* This job runs the hql that gets mediacounts from the wmf.webrequest table.

Note
* This job waits for the webrequest partition data for the hour,
* to be available before running the hql query.
* output of the hql is then put in the wmf.mediacounts table, 
'''

from datetime import datetime, timedelta
from airflow import DAG
from analytics.config.dag_config import (dataset, 
            default_args, hql_directory, artifact)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = 'mediacounts_load_hourly'
var_props = VariableProperties(f'{dag_id}_config')
source_table = 'wmf.webrequest'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2023, 3, 15)),
    schedule='@hourly',
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
    user_defined_filters=filters,
    tags=['mediacounts', 'hive', 'load', 'hourly']
) as dag:
    
    sensor = dataset('hive_wmf_webrequest_upload').get_sensor_for(dag)

    etl1 = SparkSqlOperator(
        task_id='insert_hourly_mediacounts',
        sql=var_props.get('hql_path', f'{hql_directory}/mediacounts/mediacounts_load_hourly.hql'),
        query_parameters={
            'source_table': source_table,
            'destination_table': var_props.get('destination_table', 'wmf.mediacounts'),
            'refinery_hive_jar': var_props.get('refinery_hive_jar', 
                                               artifact('refinery-hive-0.2.1-shaded.jar')),
            'coalesce_partitions': var_props.get('coalesce_partitions', 4),
            'year': '{{data_interval_start.year}}',
            'month': '{{data_interval_start.month}}',
            'day': '{{data_interval_start.day}}',
            'hour': '{{data_interval_start.hour}}'
        },
    )
    sensor >> etl1  

